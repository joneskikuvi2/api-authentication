﻿using Auth.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Auth.Providers
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {

            context.Validated();
        }
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
           // string role;
          //  string firstName;
           // string lastName;
            using (AuthRepository _repo = new AuthRepository())
            {
                ApplicationUser user = await _repo.FindUser(context.UserName, context.Password);
       
                dic.Add("Firstname", user.FirstName);
                dic.Add("Lastname", user.LastName);
                dic.Add("PhoneNumber", user.PhoneNumber);
                dic.Add("Country", user.Country);
                dic.Add("Email", user.Email);
                if (user == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
         //   identity.AddClaim(new Claim("sub", context.UserName));
           // identity.AddClaim(new Claim("role", "user"));
            var props = new AuthenticationProperties(dic);

            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);

        }
    }
}